package com.app.lecture_4_oop.students.dao;

import com.app.lecture_4_oop.students.domain.Student;

public interface MarksCalculationService {

    double getAvgStudentMark(Student student);
}
